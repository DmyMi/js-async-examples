exports.getRandom = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
}

exports.randomWithTimeOut = (max, time) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(this.getRandom(max))
        }, time);
    })
}