const util = require('./util');
const axios = require('axios');

exports.getJoke = async () => {
    const {data: categories} = await axios.get(`https://api.chucknorris.io/jokes/categories`);
    const randomStuff = await util.randomWithTimeOut(categories.length, 2000);
    return await axios.get(`https://api.chucknorris.io/jokes/random?category=${categories[randomStuff]}`)
}

exports.all = async () => {
    const {data: categories} = await axios.get(`https://api.chucknorris.io/jokes/categories`);
    return Promise.all(categories.map( cat => axios.get(`https://api.chucknorris.io/jokes/random?category=${cat}`)));
}