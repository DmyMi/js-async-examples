const request = require('request');
const util = require('./util');

exports.getSomeData = (callback) => {
    request('https://api.chucknorris.io/jokes/random', { json: true }, callback)
}


// ==============================================================================================





exports.getSomeMoreData = (callback) => {
    request('https://api.chucknorris.io/jokes/categories', { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        const catId = util.getRandom(body.length)
        console.log(`Category is: ${body[catId]}`)
        request(`https://api.chucknorris.io/jokes/random?category=${body[catId]}`, { json: true }, callback)
    });
}