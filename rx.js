const { Observable } = require('rxjs');
const { tap } = require('rxjs/operators');
const util = require('./util');

const datasource = () => {
    return new Promise((resolve, reject) => {
        const num = util.getRandom(100);
        setTimeout(() => {
            if (num % 5 === 0) {
                reject(new Error('Error happened!'));
            } else {
                resolve(num);
            }
        }, 500);
    })
}

const observer = name => ({
    next: val => console.log(`${name} next called with ${val}`),
    error: err => console.log(`${name} error called with ${err}`),
    complete: () => console.log(`${name} complete`),
})

// observable is lazy, so nothing happens till someone subscribes
// this observable generates random intervals on every subscription, so there's no point
// subscribing more then once :)
const observable = new Observable(obs => {
    const interval = setInterval(async () => {
        try {
            const n = await datasource();
            if (n < 81) {
                console.log(`Observable next: ${n}`);
                obs.next(n);
            } else {
                console.log(`Observable complete with: ${n}`);
                obs.complete();
            }

        } catch (error) {
            console.log(`Observable got error: ${error}`);
            obs.error(error);
        }
    }, 1000);

    return () => {
        // tear down logic when either error, completion or unsubscription happened
        console.log('unsubscribing!');
        clearInterval(interval);
    }
})

const subscription$ = observable
.pipe(tap(theValue => console.log(`Operator chain got: ${theValue}`)))
.subscribe(
    observer("The Observer")
);


// Uncomment this to test the unsubcribing.
// setTimeout(() => {
//     subscription$.unsubscribe();
// }, 2000);