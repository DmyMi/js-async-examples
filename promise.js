const util = require('./util');
const axios = require('axios');


// ============================================================

exports.getTheJoke = () => {
    return axios.get('https://api.chucknorris.io/jokes/categories')
        .then(
            ({data}) => {
                return data[util.getRandom(data.length)]
            }
        ).then(
            category => {
                return axios.get(`https://api.chucknorris.io/jokes/random?category=${category}`)
    }
    )
}