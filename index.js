const cb = require('./callback');
const prom = require('./promise');
const asaw = require('./asaw');

const callback = (err, res, body) => {
    if (err) { return console.log(err); }
    console.log(`Joke: ${body.value}`);
}

cb.getSomeData(callback);
cb.getSomeMoreData(callback);

// ================================

prom.getTheJoke().then(
    ({data : {value}}) => console.log(`Joke: ${value}`)
)


// ================================

async function main() {
    const {data : {value: joke}} = await asaw.getJoke();
    console.log(joke);
}

async function all() {
    const responses = await asaw.all();
    responses.forEach(({data : {value: joke}}, i) => {
        console.log(`Joke #${i+1}: ${joke}`);
    });
}

main(); // top level await without function is stage 3 but not yet implemented
all();

