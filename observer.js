class Observable {
    constructor() {
        this.observers = [];
    }

    subscribe(observer) {
        this.observers.push(observer);
    }

    unsubscribe(observer) {
        let i = this.observers.indexOf(observer);
        if (i > -1) {
            this.observers.splice(i, 1);
        }
    }

    updateInfo(value) {
        this.notifyAll(value);
    }

    notifyAll(updatedState) {
        this.observers.forEach(obs => {
            console.log(`Notified ${obs.name}`)
            obs.notify(updatedState)
        })
    }
}

class Observer {
    constructor(name) {
        this.name = name;
    }
    notify(value) {
        console.log(value)
    }
}

const interesting = new Observable(observer => {
    observer.next(42);
});
const observer = new Observer("obs1");
interesting.subscribe(observer);
interesting.updateInfo(45);
const observer1 = new Observer("obs2");
interesting.subscribe(observer1);
interesting.updateInfo(46);
interesting.unsubscribe(observer);
interesting.updateInfo(47);